#ifndef  _MYBME680_H_
#define _MYBME680_H_
#include "sys.h"

#include "bme680.h"
#include "bme680_defs.h"


//BME_IIC_SDA     PC12
//BME_IIC_SCL     PC10
//BME_IIC_ADDR    PC11     ????? 1
//CSB             PD0      ????


extern struct bme680_dev             gas_sensor;   //  设备结构体
extern struct bme680_field_data     filed_struct;
extern struct bme680_calib_data    calib_struct;
extern struct bme680_tph_sett        tph_sett_struct;
extern struct bme680_gas_sett       gas_sett_struct;
extern struct bme680_field_data     field_data_struct; 



#define BME_IIC_Dealy   1
#define BME_IIC_SCL         PAout(15)      //SCLK 
#define BME_IIC_SDA         PBout(3)      //SDA  
#define BME_READ_SDA   PBin(3)       //SDA 

#define BME_SDA_IN()      {GPIOB->MODER&=~(3<<(3*2));GPIOB->MODER|=0<<(3*2);}	//PB3????
#define BME_SDA_OUT()  {GPIOB->MODER&=~(3<<(3*2));GPIOB->MODER|=1<<(3*2);} //PB3????



int  Get_BME680_temperature(void);
int Get_BME680_presure(void);
void BME680_Init(void);
void BME_test(void);
void BME_Write_registers(u8 Reg_ADDR,u8* data_buff,u8 len);   // 裸机程序用
void BME_Read_registers(u8 Reg_ADDR,u8* data_buff,u8 len);// 裸机程序用
void Get_A_BME_Rreg(u8 reg_ADDR);// usmart  专用函数
void Set_A_BME_Rreg(u8 reg_ADDR,u8 Val);    // usmart  专用函数
void Search_IIC(void);
void my_bme_init(void);
int8_t  bme680_iic_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
int8_t  bme680_iic_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
//  以上两个函数是  库函数的 调用接口


#endif

